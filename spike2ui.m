function varargout = spike2ui(varargin)
% SPIKE2UI MATLAB code for spike2ui.fig
%      SPIKE2UI, by itself, creates a new SPIKE2UI or raises the existing
%      singleton*.
%
%      H = SPIKE2UI returns the handle to a new SPIKE2UI or the handle to
%      the existing singleton*.
%
%      SPIKE2UI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SPIKE2UI.M with the given input arguments.
%
%      SPIKE2UI('Property','Value',...) creates a new SPIKE2UI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before spike2ui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to spike2ui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help spike2ui

% Last Modified by GUIDE v2.5 16-Jan-2017 20:55:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @spike2ui_OpeningFcn, ...
                   'gui_OutputFcn',  @spike2ui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
end

%% OPEN/CLOSE

function spike2ui_OpeningFcn(hObject, eventdata, handles, varargin)
    % init this instance
    if nargin > 3,
        if isa(varargin{1}, 'spike2.ui'),
            handles.ui = varargin{1};
        else
            error('Invalid property passed to spike2ui.')
        end
    else
        optdialog('Type', 'info', ...
            'Title', 'Spike2 UI invocation', ...
            'Text', [ 'The recommended way of starting the Spike2 UI is to use spike2.ui '...
                'rather than starting spike2ui directly.'], ...
            'Buttons', {{'Ok', 'default'}}, ...
            'Program', 'matlab/spike2.ui')        
        handles.ui = spike2.ui(handles);
    end
    
    handles.ProgressBar = rectangle('Parent', handles.StatusProgress, 'Position', [0 0 0.0000000001 1], 'Edgecolor', 'none', 'Facecolor', 'b');
    
    guidata(hObject, handles);
end

function varargout = spike2ui_OutputFcn(hObject, eventdata, handles) 
    varargout{1} = handles;
end

function Window_DeleteFcn(hObject, eventdata, handles)
    % IGNORED
end

function Window_CloseRequestFcn(hObject, eventdata, handles)
    try 
        if isfield(handles, 'ui'),
            handles.ui.hide();
            return
        end
    catch exception
    end
    
    % handles.ui disappeared or something else went wrong, delete ourselves then
    delete(handles.Window);    
end

function Window_WindowButtonDownFcn(hObject, eventdata, handles)
    point = get(hObject', 'CurrentPoint');
    switch get(hObject, 'SelectionType'),
        case 'normal',
            handles.ui.mouse_button(spike2.ui.MOUSE_BUTTON_LEFT, point);
        case 'extend',
            handles.ui.mouse_button(spike2.ui.MOUSE_BUTTON_MIDDLE, point);
        case 'alt',
            handles.ui.mouse_button(spike2.ui.MOUSE_BUTTON_RIGHT, point);
        case 'open',
            handles.ui.mouse_button(spike2.ui.MOUSE_BUTTON_DOUBLECLICK, point);
    end
end

function Window_WindowButtonMotionFcn(hObject, eventdata, handles)
    handles.ui.mouse_move(get(hObject', 'CurrentPoint'));
end

function Window_WindowButtonUpFcn(hObject, eventdata, handles)
    handles.ui.mouse_button(spike2.ui.MOUSE_BUTTON_NONE, get(hObject', 'CurrentPoint'));
end

function Window_WindowScrollWheelFcn(hObject, eventdata, handles)
    handles.ui.mouse_scroll(eventdata.VerticalScrollCount);
end

function Window_WindowKeyPressFcn(hObject, eventdata, handles)
    modifier = spike2.ui.MODIFIER_KEY_NONE;
    if any(ismember(eventdata.Modifier, 'shift')), modifier = modifier | spike2.ui.MODIFIER_KEY_SHIFT; end
    if any(ismember(eventdata.Modifier, 'control')), modifier = modifier | spike2.ui.MODIFIER_KEY_CTRL; end
    if any(ismember(eventdata.Modifier, 'alt')), modifier = modifier | spike2.ui.MODIFIER_KEY_ALT; end
    handles.ui.key_press(eventdata.Character, modifier);
end

function Window_WindowKeyReleaseFcn(hObject, eventdata, handles)
    modifier = spike2.ui.MODIFIER_KEY_NONE;
    if any(ismember(eventdata.Modifier, 'shift')), modifier = modifier | spike2.ui.MODIFIER_KEY_SHIFT; end
    if any(ismember(eventdata.Modifier, 'control')), modifier = modifier | spike2.ui.MODIFIER_KEY_CTRL; end
    if any(ismember(eventdata.Modifier, 'alt')), modifier = modifier | spike2.ui.MODIFIER_KEY_ALT; end
    handles.ui.key_release(eventdata.Character, modifier);
end

%% GUI CALLBACKS
function Window_ResizeFcn(hObject, eventdata, handles)
    % SizeChangedFcn is called before OpeningFcn is finished
    if isfield(handles, 'ui'),
        handles.ui.resize();
    end
end

%% MENU CALLBACKS

function MenuFile_Callback(hObject, eventdata, handles)
    % IGNORED
end

function MenuFile_Load_Callback(hObject, eventdata, handles)
    handles.ui.load();
end

function MenuFile_Export_Callback(hObject, eventdata, handles)
    handles.ui.export();
end

function MenuItem_Close_Callback(hObject, eventdata, handles)
    handles.ui.hide();
end

function MenuFile_Exit_Callback(hObject, eventdata, handles)
    if handles.ui.changed(),
        choice = questdlg('Data has been modified, leave anyway?', 'Spike2 UI: data modified', ...
            'Ok', 'Cancel', 'Cancel');
        switch choice
            case 'Ok',
            case 'Cancel',
                return
        end
    end 
    
    handles.ui.exit();
end

%% CALLBACKS

%% AUTO-GENERATED CALLBACKS
