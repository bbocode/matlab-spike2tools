# Spike2 import and GUI tools #

## Tools ##

### spike2.file ###

Native Matlab interface for reading Spike2 files.

### spike2.ui ###

Spike2 GUI.

## Note ##

This package requires 'matlab-configtools'.