function [hdr, hdrlen] = v4_parse_linkheader(~, seq, pos)
    hdrlen = 20;
    hdr = spike2.cstruct({...
        { 'prevpos',      'uint32',   'uint32',   1 }, ... % +0000 Position of previous block
        { 'nextpos',      'uint32',   'uint32',   1 }, ... % +0004 Position of next block
        { 'starttick',    'double',   'uint32',   1 }, ... % +0008 First tick
        { 'lasttick',     'double',   'uint32',   1 }, ... % +000C Last tick
        { 'channel',      'uint32',   'uint16',   1 }, ... % +0010 Channel number
        { 'elementcount', 'uint32',   'uint16',   1 }, ... % +0012 Count of elements in this segment.
    }, seq, pos, hdrlen);
end
