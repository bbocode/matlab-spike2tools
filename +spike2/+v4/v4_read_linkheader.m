function hdr = v4_read_linkheader(obj, fid, pos)
    HDRLEN = 20;
    fseek(fid, pos, -1);
    hdr = spike2.v4.v4_parse_linkheader(obj, spike2.freadu8(fid, HDRLEN), 1);
end
