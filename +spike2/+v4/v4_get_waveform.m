function data = v4_get_waveform(obj, ch, progress_cb)
    data = struct();
    
    config = obj.channelconfig{ch};
    
    numsamples = obj.Channels{ch}.Samples;
    start = obj.Channels{ch}.Start;
    stop = obj.Channels{ch}.Stop;
    
    data.t = linspace(start, stop, numsamples);
    data.y = zeros(1, numsamples);
    
    % read data
    fid = fopen(obj.Filename);
    fseek(fid, config.firstpos, -1);
    
    cnt = 1;
    for s = 1:config.segments
        % progress
        if ~isempty(progress_cb)
            progress_cb(cnt, numsamples, ['Reading channel ' config.title]);
        end
        
        % read segment, parse link header
        buf = spike2.freadu8(fid, config.segmentsize);
        [lnk, lnklen] = spike2.v4.v4_parse_linkheader(obj, buf, 1);
        
        % insert data
        data.y(cnt:cnt+lnk.elementcount-1) = double(typecast(buf(lnklen+1:lnklen+2*lnk.elementcount), 'int16'));
        
        % advance 
        fseek(fid, lnk.nextpos, -1);
        cnt = cnt + lnk.elementcount;
    end

    if ~isempty(progress_cb)
        progress_cb(1, 1, ['Reading channel ' config.title]);
    end
    
    % finish
    fclose(fid);
end