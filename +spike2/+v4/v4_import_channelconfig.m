function v4_import_channelconfig(obj, fid)
    CONFIGOFFSET = 512; % file offset for channel config
    CONFIGLEN = 140;    % channel config block length
    
    % prepare
    obj.Channels = {};
    
    % goto channel config
    fseek(fid, CONFIGOFFSET, -1);
    
    channelconfig_x0200 = spike2.freadu8(fid, 2);       % 0200 ? unknown number
    
    num_channels = obj.header.channels; % number of channels to read
    if num_channels > 32
        error('Internal error: reading more than 32 channels has not been tested yet.')
    end
    for ch = 1:num_channels
        channel = spike2.cstruct({...
            { 'linkprev',       'uint32',   'uint32',   1 }, ... % +0000 ? Probably similar to other linked headers: the file position of previous config.
            { 'firstpos',       'uint32',   'uint32',   1 }, ... % +0004 Offset position of first data segment.
            { 'lastpos',        'uint32',   'uint32',   1 }, ... % +0008 Last segment offset.
            { 'segments',       'uint32',   'uint32',   1 }, ... % +000C Number data segments.
            { 'x0010',          'uint32',   'uint8',    4 }, ... % +0010 ? FIXME
            { 'segmentsize',    'uint32',   'uint16',   1 }, ... % +0014 Byte size of a segment.
            { 'count',          'uint32',   'uint16',   1 }, ... % +0016 Element count in first segment or sum of elements for markers.
            { 'comment',        'char',     'int8',    72 }, ... % +0018 Comment: string len 1 byte + char data.
            { 'lasttick',       'double',   'uint32',   1 }, ... % +0060 Last tick, compare datahdr.START
            { 'x0064',          'uint32',   'uint8',    4 }, ... % +0064 ? FIXME
            { 'port',           'uint32',   'uint16',   1 }, ... % +0068 Port number
            { 'title',          'char',     'int8'     10 }, ... % +006A Titel: string len 1 byte + char data
            { 'rate',           'double',   'single',   1 }, ... % +0074 Adc or Event rate
            { 'type',           'uint32',   'uint16',   1 }, ... % +0078 Channel type (see below 'Type')
            { 'scale',          'double',   'single',   1 }, ... % +007A Signal scale
            { 'offset',         'double',   'single',   1 }, ... % +007E Signal offset
            { 'unit',           'char',     'int8',    10 }, ... % +0084 Unit: string len 1 byte + char data
        }, spike2.freadu8(fid, CONFIGLEN));
        channel.comment = spike2.decodestring(channel.comment);
        channel.title = spike2.decodestring(channel.title);
        channel.unit = spike2.decodestring(channel.unit);
    
        % for debugging 
        channel = spike2.debugcstruct(channel);
        
        % common channel parameters
        switch obj.channelType(channel.type)
            case 'Off'
                % IGNORED
            otherwise
                config = struct(...
                    'Channel', ch, ...
                    'Type', obj.channelType(channel.type), ...
                    'Port', channel.port, ...
                    'Title', channel.title, ...
                    'Unit', channel.unit, ...
                    'Rate', channel.rate, ...
                    'Comment', channel.comment);
                
                % calculate number of samples, start and stop
                if channel.firstpos ~= uint32(2^32)
                    curpos = ftell(fid);
                    firstlnk = spike2.v4.v4_read_linkheader(obj, fid, channel.firstpos);
                    lastlnk = spike2.v4.v4_read_linkheader(obj, fid, channel.lastpos);
                    fseek(fid, curpos, -1);

                    % sanity checks
                    assert(firstlnk.channel == ch);
                    assert(lastlnk.channel == ch);
                    assert(lastlnk.lasttick == channel.lasttick);

                    config.Start = firstlnk.starttick * obj.TimeUnit;
                    config.Stop = lastlnk.lasttick * obj.TimeUnit;
                else
                    const.Start = 0;
                    const.Stop = 0;
                    config.Samples = 0;
                end
        end

        % specific channel parameters
        switch obj.channelType(channel.type)
            case 'Waveform'
                config.Unit = channel.unit;
                config.Offset = channel.offset;
                config.Scale = channel.scale;
                config.UnitPerBit = 5.0/2^15; % FIXME is this hardcoded
                config.Samples = (channel.segments-1)*channel.count + lastlnk.elementcount;
                config.RawDataCB = @(progress_cb) spike2.v4.v4_get_waveform(obj, ch, progress_cb);
            case 'Event-'
                config.Unit = '';
                config.RawDataCB = @(progress_cb) spike2.v4.v4_get_event(obj, ch, progress_cb);
            case 'Event+'
                config.Unit = '';
                config.RawDataCB = @(progress_cb) spike2.v4.v4_get_event(obj, ch, progress_cb);
            case 'Level'
                warning('Internal error: channel of type ''Level'' not supported');
            case 'Marker'
                config.Unit = '';
                config.RawDataCB = @(progress_cb) spike2.v4.v4_get_marker(obj, ch, progress_cb);
            case 'TextMarker'
                config.Unit = '';
                config.RawDataCB = @(progress_cb) spike2.v4.v4_get_marker(obj, ch, progress_cb);
            case 'WaveMark'
                warning('Internal error: channel of type ''WaveMark'' not supported');
        end
        
        % insert channel config into object
        switch obj.channelType(channel.type)
            case 'Off'
                % IGNORED
            otherwise
                obj.Channels{ch} = config;
        end

        % add internal channel config
        obj.channelconfig{end+1} = channel;
    end
end