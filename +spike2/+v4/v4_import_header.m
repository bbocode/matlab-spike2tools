function v4_import_header(obj, fid)
    % start at file offset 0
    fseek(fid, 0, -1);

    % convert struct
    header = spike2.cstruct({...
        { 'type',           'uint16',   'uint16',   1 }, ... % 0000-0001 Device type
        { 'copyright',      'char',     'int8',    10 }, ... % 0002-000B Copyright ('(C) CED 87')
                                                         ... %           m-(
        { 'creator',        'char',     'int8',     8 }, ... % 000C-0013 File creator ('S2061933')
        { 'timeunit',       'double',   'uint16',   1 }, ... % 0014-0015 Microseconds per time unit
        { 'time_per_adc',   'double',   'uint16',   1 }, ... % 0016-0017 FIXME use unknown
        { 'filestate',      'uint16',   'uint16',   1 }, ... % 0018-0019 FIXME use unknown
        { 'dataoffset',     'int64',    'uint32',   1 }, ... % 001A-001D Data offset in file
        { 'channels',       'int64',    'uint16',   1 }, ... % 001E-001F Number of channels
        { 'channel_size',   'int64',    'uint16',   1 }, ... % 0020-0021 FIXME use unknown
        { 'extra_data',     'int64',    'uint16',   1 }, ... % 0022-0023 FIXME use unknown
        { 'buffersize',     'int64',    'uint16',   1 }, ... % 0024-0025 FIXME use unknown
        { 'os_format',      'uint16',   'uint16',   1 }, ... % 0026-0027 FIXME format unknown
        { 'max_ftime',      'int64',    'uint32',   1 }, ... % 0028-002B FIXME use unknown
        { 'time_base',      'double',   'double',   1 }, ... % 002C-0033 Time unit base (seconds)
                                                         ... %      Old devices (prior to 'type' 6) use 1/1 MHz resolution.
        { 'x0034',          'uint8',    'uint8',    1 }, ... % 0034-0034 FIXME date related - unknown format
        { 'second',         'uint8',    'uint8',    1 }, ... % 0035-0035 FIXME date related - unknown format (seconds?)
        { 'minute',         'double',   'uint8',    1 }, ... % 0036-0036 Minute
        { 'hour',           'double',   'uint8',    1 }, ... % 0037-0037 Hour
        { 'day',            'double',   'uint8',    1 }, ... % 0038-0038 Day
        { 'month',          'double',   'uint8',    1 }, ... % 0039-0039 Month
        { 'year',           'double',   'uint16',   1 }, ... % 003A-003B Year
        { 'pad',            'uint8',    'uint8',   52 }, ... % 003C-006F [padded, obivously for future use]
                                                         ...
        { 'comments',       'uint8',    'uint8',  400 }  ... % 003C-0200 File comments
                                                             %           5 lines each 80 byte long:
                                                             %           - first byte designates string length
                                                             %           - the rest is character data
    }, spike2.freadu8(fid, 512));

    % for debugging 
    header = spike2.debugcstruct(header);

    % sanity checks
    assert(header.os_format == 0);
    
    % channel type
    switch header.type
        case 1
            obj.HardwareType = 'General compatibility';
        case 2
            obj.HardwareType = '1401plus old ADC';
        case 3
            obj.HardwareType = '1401plus';
        case 4
            obj.HardwareType = 'Micro1401';
        case 5
            obj.HardwareType = 'Micro1401 mkII';
        case 6
            obj.HardwareType = 'Power1401';
        case 7
            obj.HardwareType = 'Power1401 625kHz';
        case 8
            obj.HardwareType = 'Power1401 mkII';
        case 9
            obj.HardwareType = 'Micro1401-3';
        otherwise
            warning('Unknown CED 1401 type. Parameters have been guessed and may be wrong!');
    end

    % update object
    obj.header = header;
    
    % timeunit, using seconds
    obj.ClockRate = 1e6;
    if header.type > 5
        obj.ClockRate = 1/header.time_base;
    end
    obj.TimeUnit = header.timeunit / obj.ClockRate;
    obj.SamplingRate = 1/obj.TimeUnit;

    obj.TimeStamp = sprintf('%04d%02d%02d-%02d%02d%02d', ...
        header.year, header.month, header.day, ...
        header.hour, header.minute, header.second);
end
