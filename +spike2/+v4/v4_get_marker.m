function data = v4_get_marker(obj, ch, progress_cb)
    data = struct();
    
    config = obj.channelconfig{ch};

    data.dt = obj.Channels{ch}.Rate/1e6; % FIXME related to 1MHz or 10Mhz system clock
    data.t = [];
    data.marker = [];

    if config.firstpos == uint32(2^32)
        % no events recorded
        return
    end
    
    % read data
    fid = fopen(obj.Filename);
    fseek(fid, config.firstpos, -1);
    
    cnt = 1;
    for s = 1:config.segments
        % progress
        if ~isempty(progress_cb)
% FIXME           progress_cb(cnt, numsamples, ['Reading channel ' config.title]);
        end
        
        % read segment, parse link header
        buf = spike2.freadu8(fid, config.segmentsize);
        [lnk, lnklen] = spike2.v4.v4_parse_linkheader(obj, buf, 1);
        
        % insert data
        pos = lnklen+1;
        for m = 1:lnk.elementcount
            data.t(end+1) = double(typecast(buf(pos:pos+3), 'uint32')) * obj.TimeUnit;
            data.marker(end+1,:) = buf(pos+4:pos+7);

            pos = pos+8;
        end
    
        % advance 
        fseek(fid, lnk.nextpos, -1);
        cnt = cnt + lnk.elementcount;
    end
    
    % progress
    if ~isempty(progress_cb)
        progress_cb(1, 1, ['Reading channel ' config.title]);
    end

    % finish
    fclose(fid);
end