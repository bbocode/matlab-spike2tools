function s = decodestring(b)
% MAKESTRING - Spike string encoding: first byte is string length.
    % clamp string
    b = b(2:2+b(1)-1);
    
    % handle nul-termination if present
    nul = min(find(b == 0)); %#ok<MXFND>
    if isempty(nul)
        nul = length(b)+1;
    end
    s = char(b(1:nul-1));
end

