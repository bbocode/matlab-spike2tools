classdef ui < handle
% spike2.ui - Spike2 user interface.
%
% USAGE
% FIXME
    %% Enumerations
    properties(Constant,Hidden)
        MOUSE_BUTTON_NONE           = 0
        MOUSE_BUTTON_LEFT           = 1
        MOUSE_BUTTON_MIDDLE         = 2
        MOUSE_BUTTON_RIGHT          = 3
        MOUSE_BUTTON_DOUBLECLICK    = 4
        
        MODIFIER_KEY_NONE           = 0
        MODIFIER_KEY_SHIFT          = 1
        MODIFIER_KEY_CTRL           = 2
        MODIFIER_KEY_ALT            = 4
        
        Application = 'spike2.ui';
        Version = '1.0';
    end

    %% Constructor
    methods
        function obj = ui(handles)
        % Constructor
            if nargin > 0
                if isstruct(handles) && ishandle(handles.Window)
                    [~,n,e] = fileparts(get(handles.Window, 'FileName'));
                    switch [n e]
                        case 'spike2ui.fig'
                            obj.UI = handles;
                        otherwise
                            error('Invalid figure passed to spike2.ui (expected spike2ui.fig).')
                    end
                else
                    error('Invalid arguments passed to spike2.ui.');
                end
            else
                obj.UI = spike2ui(obj);
                obj.UI.ui = []; % remove cyclic reference
            end
            
            obj.Config = appconfig('Program', ['spike2.ui']);
            
            % init
            obj.resize();
        end
        
        function delete(self)
        % Destructor
            if ~isempty(self.UI.Window)
                fig = self.UI.Window; % avoid multiple entries
                self.UI.Window = []; 
                
                if ishandle(fig)
                    delete(fig);   % delete non-singleton figure
                end
            end
        end
    end
    
    %% properties
    % public properties
    properties
        File = []               % Spike2 file (handle to spike2.file).
        Data = struct(...       % (Exportable) data.
            'Application', spike2.ui.Application, ...
            'Version', spike2.ui.Version, ...
            'Source', [], ...       % Spike2 file name.
            'Tags', {}, ...         % Channel tags (converted from title).
            'Titles', {}, ...       % Channel titles.
            'Channel', {})          % Channel data (in order of Tags/Titles).
        Selection = struct(...      % Publicly available selection (such as a level).
            'Event', [], ...            % event type 
            'Channel', [], ...          % channel in which event happend
            'Data', []);                % event data
    end
    
    % GUI and internal properties
    properties % FIXME (Access = private) or (Hidden)
        Config = []             % Application config.
        
        UI = []                 % GUI handles.
        External = struct(...   % External handles and data.
            'Marker', [], ...       % markers
            'DUMMY', []), ...
        Axes = struct(...       % Displayed axes and associated data.
            'T', [0 0],...          % displayed time
            'Selected', 0,...       % emphasized axis
            'Handles', [], ...      % all handles
            'Position', [], ...     % layout
            'Properties', [], ...   % axes properties
            'Label', [], ...        % y-axis label
            'Type', ''), ...        % display type ('marker', 'plot')
        Status = { '' }         % Status messages.
        Layout = []             % GUI layout data.
        Input = struct(...      % Mouse and keyboard input.
            'Button', 0, ...           % Currently pressed mouse button.
            'SavedPosition', [0 0 0 0], ...    % Last saved mouse coordinate.
            'Position', [0 0 0 0], ... % Current mouse position.
            'Modifier', 0,....         % Currently pressed modifier key.
            'ScrollCount', 0, ...      % Scrolling count.
            'ScrollEvents', 0)         % Number of unprocess scroll events.
    end

    %% methods: general
    methods
        function res = changed(self) %#ok<MANU>
        % Check if data is changed.
            res = false;
        end

        function load(self, fname)
        % Load data (possibly discarding old data).
            % if no filename given, use uigetfile
            if nargin < 2
                [fn,p] = uigetfile({...
                    '*.smr;*.smrx;*.mat','Spike2 data (raw or exported)'; ...
                    '*.smr;*.smrx','Spike2 data'; ...
                    '*.mat', 'Exported Spike2 data'}, ...
                    'Select data file....', ...
                    self.Config.get('recent.path', '.'));

                if fn
                    self.Config.set('recent.path', p);
                    fname = fullfile(p, fn);
                else
                    % no file selected
                    return
                end
            end

            % clear display
            self.clearData();
            
            % load data
            self.pushStatus(['Loading ''' fname '''.']);
            
            [~, ~, ext] = fileparts(fname);
            switch ext
                case {'.smr', '.smrx'}
                    self.File = spike2.file(fname);
                    self.title(fname);
                    
                    if self.File.valid()
                        % reset Data field
                        self.Data = struct();
                        self.Data.Source = struct();
                        self.Data.Tags = {};
                        self.Data.Titles = {};
                        self.Data.Channel = {};
                
                        % source
                        [~, fname, fext] = fileparts(self.File.Filename);
                        self.Data.Source.Filename = [fname fext];
                        self.Data.Source.Version = self.File.Version;
                        self.Data.Source.HardwareType = self.File.HardwareType;
                        self.Data.Source.TimeUnit = self.File.TimeUnit;
                        self.Data.Source.SamplingRate = self.File.SamplingRate;
                        self.Data.Source.TimeStamp = self.File.TimeStamp;
                        
                        % load all channels
                        self.Data.Titles = self.File.channelTitles();
                        self.Data.Tags = matlab.lang.makeValidName(matlab.lang.makeUniqueStrings(self.File.channelTitles()));
                        
                        tlimits = [inf -inf];
                        for t = 1:length(self.Data.Titles)
                            self.pushStatus(['Reading channel ''' self.Data.Titles{t} '''...']);

                            ch = self.File.channelByTitle(self.Data.Titles{t});
                            self.Data.Channel{end+1} = self.File.getByChannel(ch, @(i,N, msg) self.progress(double(i)/double(N)));

                            % time axis
                            tlimits = [min([self.Data.Channel{end}.Data.t tlimits(1)]) max([self.Data.Channel{end}.Data.t tlimits(2)])];
                            
                            % add y if not present
                            tidx = size(self.Data.Channel{end}.Data.t);
                            switch self.Data.Channel{end}.Type
                                case 'Waveform'
                                case 'Event+'
                                    self.Data.Channel{end}.Data.y = ones(tidx);
                                case 'Event-'
                                    self.Data.Channel{end}.Data.y = -ones(tidx);
                                case 'Level'
                                    % FIXME self.Data.Channel{end}.Data.y = ones(tidx);
                                case 'Marker'
                                    self.Data.Channel{end}.Data.y = zeros(tidx);
                                case 'WaveMark'
                                    self.Data.Channel{end}.Data.y = zeros(tidx);
                                case 'TextMarker'
                                    self.Data.Channel{end}.Data.y = zeros(tidx);
                                otherwise
                                    error(['Internal error: invalid or unknown channel type ''' self.Data.Channel{k}.Type '''.'])
                            end
                            
                            self.popStatus();
                        end
                        self.Data.Source.Time = tlimits;
                        
                        self.progress(0);
                    end
                    
                case '.mat'
                    self.File = [];
                    self.Data = load(fname);
                    if ~strcmp(self.Data.Application, 'spike2.ui')
                        self.clearData();
                        error('Loading failed: not spike2 data');
                    end
                    if ~strcmp(self.Data.Version, spike2.ui.Version)
                        % FIXME
                        error('Loading failed: version mismatch');
                    end
            end
            
            self.clearStatus();
            
            % start displaying data
            self.t('all');
        end

        function res = channelId(self, tag)
        % Find channel number by title or tag.
            res = find(strcmp(self.Data.Titles, tag));
            if isempty(res)
                res = find(strcmp(self.Data.Tags, tag));
            end
        end
        
        function export(self)
        % Export data.
            if isempty(self.Data) || isempty(self.Data.Source)
                return
            end
            [~,fn,~] = fileparts(self.Data.Source.Filename);

            [fn,p] = uiputfile(fullfile(self.Config.get('recent.savepath', '.'), [fn '.mat']), ...
                'Export Spike2 data....');

            if fn
                self.Config.set('recent.savepath', p);
                fname = fullfile(p, fn);
            else
                % no file selected
                return
            end
            
            self.pushStatus(['Exporting data (' fname ')...']);
            
            f = matfile(fname, 'Writable', true);
            
            f.Application = 'spike2.ui';
            f.Version = spike2.ui.Version;
            
            f.Source = self.Data.Source;
            f.Tags = self.Data.Tags;
            f.Titles = self.Data.Titles;
            f.Channel = self.Data.Channel;
            
            self.clearStatus();
        end
        
        function setExternalMarker(self, channel, data)
        % Set 'external' markers to a trace.
            if ischar(channel)
                found = self.channelId(channel);
                if isempty(found)
                    if strcmpi(channel, 'all')
                        for k=1:numel(self.Data.Titles)
                            self.setExternalMarker(k, data);
                        end
                    else
                        error(['Invalid channel ''' channel '''.']);
                    end
                end
                channel = found;
            end
            
            if nargin < 3
                self.External.Marker{channel} = {};
            else
                self.External.Marker{channel} = { data };
            end
            self.showData();
        end
        
        function addExternalMarker(self, channel, data)
        % Add 'external' markers to a trace.
            if ischar(channel)
                found = self.channelId(channel);
                if isempty(found)
                    error(['Invalid channel ''' channel '''.']);
                end
                channel = found;
            end
            
            self.External.Marker{channel}{end+1} = data;
            self.showData();
        end
        
    end
    
    %% methods: UI and callbacks
    methods
        function show(self)
        % Show UI.
            set(self.UI.Window, 'Visible', 'on');
        end
        
        function hide(self)
        % Hide UI.
            set(self.UI.Window, 'Visible', 'off');
        end
        
        function select(self, which)
        % Highlight a certain axis.
            if ischar(which)
                if strcmp(which, 'none')
                    found = 0;
                else
                    found = self.channelId(which);
                    if isempty(found)
                        error(['Invalid channel ''' which '''.']);
                    end
                end
                which = found;
            end
            
            if which < 0 || which > length(self.Data.Titles)
                error('Invalid channel number %d', which)
            end
            
            % set background
            if which == 0
                for a = 1:length(self.Axes.Handles)
                    set(self.Axes.Handles{a}, 'Selected', 'off');
                end
            else
                for a = 1:length(self.Axes.Handles)
                    if which == a
                        set(self.Axes.Handles{a}, 'Selected', 'on');
                    else
                        set(self.Axes.Handles{a}, 'Selected', 'off');
                    end
                end
            end
        end
        
        function varargout = axesVisible(self, which, val)
        % Get or set axes visiblity.
            if ischar(which)
                found = self.channelId(which);
                if isempty(found)
                    error(['Invalid channel ''' which '''.']);
                end
                which = found;
            end
            
            if which < 1 || which > length(self.Data.Titles)
                error('Invalid channel number %d', which)
            end
            
            if nargin == 2
                switch get(self.Axes.Handles{which}, 'Visible')
                    case 'on'
                        varargout{1} = true;
                    otherwise
                        varargout{1} = false;
                end
            else
                if islogical(val)
                    if val
                        val = 'on';
                    else
                        val = 'off';
                    end
                end
                
                set(self.Axes.Handles{which}, 'Visible', val);
                children = get(self.Axes.Handles{which}, 'Children');
                for c = 1:numel(children)
                    set(children(c), 'Visible', val);
                end
                
                self.recalculateLayout()
            end
        end
        
        function title(self, txt)
        % Add supplemental message to the window title ("XYZ UI: txt").
            s = strsplit(get(self.UI.Window, 'Name'), ':');
            if iscell(s)
                s = s{1};
            end
            
            if nargin < 2 || isempty(txt)
                set(self.UI.Window, 'Name', [s]); %#ok<NBRAK>
            else
                set(self.UI.Window, 'Name', [s ': ' txt]);
            end
        end
        
        function parprogress(self, n)
        % Show progress for parfor-loops.
            persistent N
            persistent cnt
            
            if islogical(n)
                cnt = cnt + 1;

                if cnt/N < 0
                    error('Ehhhh?')
                end
                self.progress(cnt/N);
            else
                N = n;
                cnt = 0;
                
                if n == 0
                    self.progress(0);
                end
            end
        end
        
        function progress(self, ratio)
        % Show progress.
            ratio = max([1e-6 ratio]); % older versions of Matlab cannot have width 0
            pos = get(self.UI.ProgressBar, 'Position');
            
            if abs(pos(3)-ratio) > 0.01 % avoiding updates to occur to often (like when reading a file with >10e6 data points)
                set(self.UI.ProgressBar, 'Position', [0 0 ratio 1]);
                drawnow;
            end
        end

        function pushStatus(self, msg)
        % Push status message.
            self.Status{end+1} = msg;
            set(self.UI.StatusText, 'String', self.Status{end});
            drawnow;
        end
        
        function changeStatus(self, msg)
        % Push status message.
            self.Status{end} = msg;
            set(self.UI.StatusText, 'String', self.Status{end});
            drawnow;
        end
        
        function popStatus(self)
        % Pop last status
            if length(self.Status) > 1
                self.Status = self.Status(1:end-1);
            end
            set(self.UI.StatusText, 'String', self.Status{end});
            drawnow;
        end
        
        function clearStatus(self)
        % Clear status messages.
            self.Status = {''};
            set(self.UI.StatusText, 'String', self.Status{end});
            drawnow;
        end
        
        function resize(self)
        % Resize (SizeChangedFcn) logic for GUI.
            if isempty(self.UI)
                % self.UI has not been assigned (resize happens before
                % OutputFcn is called)
                return 
            end
        
            % Initialize layout
            if isempty(self.Layout)
                self.initLayout();
            end
            
            % Recalculate layout.
            self.recalculateLayout();
        end        
        
        function exit(self)
        % Exit UI.
            delete(self);
        end
        
        function varargout = t(self, arg)
        % Set or get time limits to display.
        %
        % USAGE
        %   obj.t('all')
        %   obj.t([tmin tmax])
        %   ans = obj.t()
            if nargin < 2
                varargout{1} = self.Axes.T;
            else
                if ischar(arg)
                    switch lower(arg)
                        case 'all'
                            self.Axes.T = self.Data.Source.Time;
                        otherwise
                            error('Invalid argument passed to method t()');
                    end
                elseif isnumeric(arg) && length(arg == 2) && (arg(1) < arg(2))
                    if arg(1) < self.Data.Source.Time(1)
                        arg(1) = self.Data.Source.Time(1);
                    end
                    if arg(2) > self.Data.Source.Time(2)
                        arg(2) = self.Data.Source.Time(2);
                    end
                    self.Axes.T = arg;
                else
                    error('Invalid argument passed to method t()');
                end
            end
            
            self.showData();
        end
    end

    %% methods: UI button and key callbacks
    methods
        function mouse_button(self, btn, point)
            self.mouse_where(point);
            
            self.Input.Position = point;
            self.Input.SavedPosition = point;
            self.Input.Button = btn;
            
            switch btn
                case self.MOUSE_BUTTON_RIGHT
                    % FIXME context menu
                case self.MOUSE_BUTTON_DOUBLECLICK
                    % FIXME use double-click?
                otherwise
                    % IGNORED
            end
        end
        
        function mouse_move(self, point)
            self.Input.Position = point;
            [t, ch] = self.mouse_where(point);
            
            if isnan(t)
                if isnan(ch)
                    return
                end
            end
        end
        
        function mouse_scroll(self, dir)
            self.Input.ScrollEvents = self.Input.ScrollEvents + 1; % poor man's semaphore
            self.Input.ScrollCount = self.Input.ScrollCount + dir;
            
            if self.Input.ScrollEvents == 1
                [t, ch] = self.mouse_where(self.Input.Position); %#ok<ASGLU>
                
                Trange = (self.Axes.T(2)-self.Axes.T(1));
                Tscroll = self.Axes.T(1) + t * Trange;

                % zoom in/out
                Trange = Trange * 2^self.Input.ScrollCount;
                self.Input.ScrollCount = 0;

                Tlow = Tscroll - t*Trange;
                Thi = Tscroll + (1-t)*Trange;
                
                if Tlow < self.Data.Source.Time(1)
                    Tlow = self.Data.Source.Time(1);
                end
                if Thi > self.Data.Source.Time(2)
                    Thi = self.Data.Source.Time(2);
                end
                
                self.t([Tlow Thi]);
            end
            
            self.Input.ScrollEvents = self.Input.ScrollEvents - 1;
        end
        
        function key_press(self, key, modifier) %#ok<INUSL,INUSD>
            if ~isempty(key)
                % IGNORE (handle via hot keys in GUI)
            else
%                fprintf(1, 'KEY PRESS   %d %d\n', double(key), modifier)
            end
        end
        
        function key_release(self, key, modifier) %#ok<INUSL,INUSD>
            if ~isempty(key)
                % IGNORE (handle via hot keys in GUI)
            else
%                fprintf(1, 'KEY RELEASE %d %d\n', double(key), modifier)
            end
        end
    end

    %% methods: internal
    methods
        function initLayout(self)
            statusbar_position = get(self.UI.StatusBar, 'Position'); % only height of statusbar is fixed
            
            self.Layout = struct(...
                'Units', 'characters', ...      % Default units: characters
                'AxesSpacing', struct(...       % Geometry/layout for axes 
                    'Top', 0.5, ...                 % top of axes
                    'Bottom', 1.5, ...              % bottom of axes
                    'Left', 10, ...                  % left of axes
                    'Right', 0.5), ...              % right of axes
                'StatusBar', struct(...         % StatusBar geometry
                    'Height', statusbar_position(4)), ...
                'Elements', struct(...          % Default (minimum) sizes for different control elements.
                    'CheckBox', [0 0 2.5 1.5]),...  
                'MDI', struct(...               % Multi-document interface (plot area)
                    'Position', []), ...            % calculated in recalculaploteLayout()
                'Dummy', []);
        end
        
        function recalculateLayout(self)
            % update Window geometry
            window_position = get(self.UI.Window, 'Position');
            self.Layout.Window.Width = window_position(3);
            self.Layout.Window.Height = window_position(4);

            %%% statusbar and its components
            set(self.UI.StatusBar, 'Position', [ 0 0 self.Layout.Window.Width self.Layout.StatusBar.Height ]);
            
            pos = get(self.UI.StatusText, 'Position');
            set(self.UI.StatusText, 'Position', [pos(1) pos(2) self.Layout.Window.Width pos(4)]);
            
            pos = get(self.UI.StatusProgress, 'Position');
            set(self.UI.StatusProgress, 'Position', [pos(1) pos(2) self.Layout.Window.Width pos(4)]);
            
            %%% Data
            self.Layout.MDI.Position = [ 0 self.Layout.StatusBar.Height ...
                self.Layout.Window.Width (self.Layout.Window.Height - self.Layout.StatusBar.Height)];
            
            if ~isempty(self.Data)
                N = length(self.Data.Titles);

                % count plots, markers
                Nplot = 0;
                Nmarker = 0;
                for k = 1:N
                    if self.axesVisible(k)
                        switch self.Axes.Type{k}
                            case 'marker'
                                Nmarker = Nmarker + 1;
                            case 'plot'
                                Nplot = Nplot + 1;
                        end
                    end
                end

                % calculate heights
                pos = [ self.Layout.AxesSpacing.Left  (self.Layout.StatusBar.Height+self.Layout.AxesSpacing.Bottom) ...
                    (self.Layout.Window.Width-self.Layout.AxesSpacing.Left-self.Layout.AxesSpacing.Right) 0];

                Hmarker = 1.5; % FIXME put in Layout struct
                Hplot = self.Layout.Window.Height...
                    - self.Layout.StatusBar.Height ...
                    - (Nmarker+Nplot) * self.Layout.AxesSpacing.Top ...
                    - self.Layout.AxesSpacing.Bottom ...
                    - Nmarker * Hmarker;
                Hplot = Hplot / Nplot;

                % calculate positions
                for k = N:-1:1
                    if self.axesVisible(k)
                        switch self.Axes.Type{k}
                            case 'marker'
                                h = Hmarker;
                            case 'plot'
                                h = Hplot;
                        end
                        pos(4) = h;
                        self.Axes.Position{k} = pos;

                        % next
                        pos(2) = pos(2) + h + self.Layout.AxesSpacing.Top;
                    end
                end

                % assign positions
                for k = 1:N
                    if self.axesVisible(k)
                        set(self.Axes.Handles{k}, 'Position', self.Axes.Position{k}, 'Units', self.Layout.Units);
                    end
                end
            end
        end
        
        function plotWaveform(self, ax, data)
            mpos = get(0, 'MonitorPositions');
            maxDataPoints = 2*max(mpos(:,3));

            % show reduced number of points in plot
            idx = find((data.t >= self.Axes.T(1)) & (data.t <= self.Axes.T(2)));
            Nidx = length(idx);
            if Nidx > maxDataPoints
                % only plot a certain amount of data points, because
                % the plot function becomes horribly slow for many data points
                % which are quite common in Spike2 files
                bin = floor(Nidx/maxDataPoints);
                parts = ceil(Nidx/bin);

                len = bin*parts;
                yh = reshape([data.y(idx) zeros(1, len - Nidx)], bin, parts);

                y = zeros(1, 2*parts);
                y(1:2:end) = min(yh, [], 1);
                y(2:2:end) = max(yh, [], 1);

                t = linspace(data.t(idx(1)), data.t(idx(end)), 2*parts); % FIXME is this correct?

                plot(ax, t, y);
            else
                plot(ax, data.t(idx), data.y(idx));
            end
        end
        
        function plotMarker(self, ax, data, what) %#ok<INUSL>
            if nargin < 4
                what = '';
            end

            t = data.t;
            y = data.y;
            if isfield(data, 'format')
                fmt = data.format;
            else
                fmt = '+';
            end
            
            M = length(t);
            
            plot(ax, t, y, fmt);
            if isfield(data, 'marker') && ~isempty(data.marker)
                marker = data.marker(:,1);

                labels = cell(1, M);
                for n = 1:M
                    switch what
                        otherwise
                            % printable chars or hex otherwise
                            ch = marker(n);
                            if (ch >= 33) && (ch <= 126)
                                labels{n} = char(ch);
                            elseif (ch == 32)
                                labels{n} = sprintf('''%s''', char(ch));
                            else
                                labels{n} = sprintf('0x%02X', ch);
                            end
                    end
                end
                text(t, y, labels, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'Parent', ax);
            end
        end
        
        function clearData(self)
            if ~isempty(self.Axes.Handles)
                for k = 1:length(self.Axes.Handles)
                    delete(self.Axes.Handles{k});
                end
            end
            self.Axes.Handles = {};
        end
        
        function initializeData(self)
        % Initialize 
            if ~isempty(self.Axes.Handles)
                error('Internal error: initializeData() called before clearData().');
            end
                        
            N = length(self.Data.Titles);
            
            % init structure
            self.Axes.Handles = cell(1,N);
            self.Axes.Position = cell(1,N);
            self.Axes.Type = cell(1,N);
            self.Axes.Properties = cell(1,N);
            self.Axes.Label = cell(1,N);
            
            self.External.Marker = cell(1,N);

            ismarker = 0; % swap ylabel lines to avoid overlapping text in the figure
            
            % create axes per channel
            for k = 1:N
                % create axes
                ax = axes('Units', self.Layout.Units, 'Position', [0 0 1e-6 1e-6], 'Parent', self.UI.Window);

                % fill internal structure (and plot data for certain channels)
                self.Axes.Handles{k} = ax;
                self.Axes.Position{k} = [0 0 1e-6 1e-6];
                switch self.Data.Channel{k}.Type
                    case 'Waveform'
                        self.Axes.Type{k} = 'plot';
                        self.Axes.Properties{k} = { 'YTickMode', 'auto' };
                    case 'Event+'
                        self.Axes.Type{k} = 'marker';
                        self.Axes.Properties{k} = { 'YTick', [1], 'YTickMode', 'manual' }; %#ok<NBRAK>
                    case 'Event-'
                        self.Axes.Type{k} = 'marker';
                        self.Axes.Properties{k} = { 'YTick', [-1], 'YTickMode', 'manual' }; %#ok<NBRAK>
                    case 'Level'
                        self.Axes.Type{k} = 'marker';
                        self.Axes.Properties{k} = { 'YTick', [0.0], 'YTickMode', 'manual' }; %#ok<NBRAK> FIXME
                    case 'Marker'
                        self.Axes.Type{k} = 'marker';
                        self.Axes.Properties{k} = { 'YTick', [], 'YTickMode', 'manual', 'YLimMode', 'manual', 'YLim', [-1 2.5] };
                    case 'WaveMark'
                        self.Axes.Type{k} = 'marker';
                        self.Axes.Properties{k} = { 'YTick', [], 'YTickMode', 'manual' };
                    case 'TextMarker'
                        self.Axes.Type{k} = 'marker';
                        self.Axes.Properties{k} = { 'YTick', [], 'YTickMode', 'manual' };
                end
                
                % axes properties (only last axes has time axis)
                set(ax, self.Axes.Properties{k}{:});
                if k ~= N
                    set(ax, 'XTick', [], 'XTickMode', 'manual');
                else
                    set(ax, 'XTickMode', 'auto');
                end

                % ylabel
                switch self.Axes.Type{k}
                    case 'marker'
                        if mod(ismarker,2) == 0
                            self.Axes.Label{k} = {self.Data.Channel{k}.Title; ''};
                        else
                            self.Axes.Label{k} = {'', self.Data.Channel{k}.Title};
                        end
                        ismarker = ismarker + 1;
                    case 'plot'
                        if ~isempty(self.Data.Channel{k}.Unit)
                            self.Axes.Label{k} = {self.Data.Channel{k}.Title; [ '(' self.Data.Channel{k}.Unit ')' ]};
                        else
                            self.Axes.Label{k} = {self.Data.Channel{k}.Title; ''};
                        end
                end
                ylabel(ax, self.Axes.Label{k}, 'Interpreter', 'none', 'FontSize', 8, 'FontWeight', 'bold');
            end
                        
            self.recalculateLayout();
        end
        
        function showData(self)
            if isempty(self.Axes.Handles)
                self.initializeData();
            end
            
            for k=1:length(self.Axes.Handles)
                if self.axesVisible(k)
                    ax = self.Axes.Handles{k};

                    % plot data
                    switch self.Axes.Type{k}
                        case 'marker'
                            % already plotted in initializeData()
                            self.plotMarker(ax, self.Data.Channel{k}.Data);
                            if ~isempty(self.External.Marker{k})
                                hold(ax, 'on');
                                for mm = 1:numel(self.External.Marker{k})
                                    self.plotMarker(ax, self.External.Marker{k}{mm});
                                end
                                hold(ax, 'off');
                            end
                            set(ax, 'XLim', self.Axes.T);
                        case 'plot'
                            % re-plot data
                            self.plotWaveform(ax, self.Data.Channel{k}.Data);
                            if ~isempty(self.External.Marker{k})
                                hold(ax, 'on');
                                for mm = 1:numel(self.External.Marker{k})
                                    self.plotMarker(ax, self.External.Marker{k}{mm});
                                end
                                hold(ax, 'off');
                            end
                            set(ax, 'XLim', self.Axes.T);
                            ylabel(ax, self.Axes.Label{k}, 'Interpreter', 'none', 'FontSize', 8, 'FontWeight', 'bold');
                    end
                end
            end
        end
        
        function [trel, ch, region] = mouse_where(self, point)
            trel = NaN;
            ch = NaN;
                
            if isempty(self.Axes.Position) || (point(2) < self.Layout.StatusBar.Height)
                % no plot are or outside
                region = 'outside';
                set(self.UI.Window, 'Pointer', 'arrow');
                return
            end
            
            if self.iswithin(point(1), [self.Axes.Position{1}(1) self.Axes.Position{1}(1)+self.Axes.Position{1}(3)])
                % on time axis (relative position)
                trel = (point(1)-self.Axes.Position{1}(1))/self.Axes.Position{1}(3);
            else
                trel = NaN;
            end
            
            for k = 1:length(self.Axes.Position)
                if self.iswithin(point(2), [self.Axes.Position{k}(2) self.Axes.Position{k}(2)+self.Axes.Position{k}(4)])
                    ch = k;
                    region = 'axes';
                    set(self.UI.Window, 'Pointer', 'crosshair');
                    return
                end
            end
            ch = 0;
            if ~isnan(trel)
                set(self.UI.Window, 'Pointer', 'hand');
            else
                set(self.UI.Window, 'Pointer', 'arrow');
            end
        end
    end
    
    %% Utilities
    methods(Static)
        function r = iswithin(x,range)
           r = (x>=range(1)) & (x<=range(2));
        end
    end
end








