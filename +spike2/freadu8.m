function res = freadu8(fid,n)
    % FREADU8 - Read sequence of bytes (uint8).
    res = uint8(fread(fid, n, 'uint8')');
end
