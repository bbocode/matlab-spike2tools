classdef file < handle
% spike2.file - Spike2 file importer.
%
% USAGE
% FIXME
    %% Constructor
    methods
        function obj = file(fname)
        % Constructor
            % prepare
            obj.Channels = {};
            
            obj.config = appconfig('Program', 'spike2tools.spike2');
            obj.header = struct();
            obj.channelconfig = {};

            if nargin < 1
                obj.open();
            else
                obj.open(fname);
            end
        end
    end
    
    %% Properties
    properties
        Filename              % Spike2 (smr) file name.
        
        Version               % File version.
        HardwareType          % Hardware type.
        TimeUnit              % Sampling time unit (s)
        SamplingRate          % Samplng rate (Hz)
        TimeStamp             % ISO timestamp
        ClockRate             % Clock rate.
        
        Channels              % List of used channels.
    end
    
    properties(Hidden)
        config        % application config
        
        header        % raw file header
        channelconfig % raw channel config
    end
    
    %% Methods
    methods
        function res = valid(self)
         % valid - Check if file or filename is/was valid.
             res = ~~self.Filename;
        end
        
        function res = open(self, varargin)
            % arguments
            if nargin < 2
                % no filename given, use uigetfile
                [fn,p] = uigetfile({'*.smr;*.smrx','Spike2 data'}, ...
                    'Select file....', ...
                    self.config.get('recent.path', '.'));
                
                if fn
                    self.config.set('recent.path', p);
                    self.Filename = fullfile(p, fn);
                else
                    self.Filename = false;
                end
            else
                % use given filename
                self.Filename = varargin{1};
            end
            
            % import data
            self.import();
            
            res = self.valid();
        end
         
        function import(self)
        % import - Import data (called by constructor).
            if ~self.valid()
                return
            end
        
            fid = fopen(self.Filename);
            if fid < 0
                warning(['No such file ''' self.Filename '''.']);
                self.Filename = false;
                return
            end

            % reset fields
            self.Version = 'v4'; % FIXME detect file version
            self.HardwareType = '';
            self.TimeUnit = NaN;
            self.SamplingRate = NaN;
            self.TimeStamp = '';
            self.Channels = {};
            self.header = [];
            self.channelconfig = {};
            
            % read file
            switch self.Version
                case 'v4'
                    spike2.v4.v4_import_header(self, fid);
                    spike2.v4.v4_import_channelconfig(self, fid);
            end
            
            % finish
            fclose(fid);
        end
        
        function res = activeChannels(self)
        % activeChannels - Get array of active channels.
            res = [];
            for k = 1:length(self.Channels)
                if ~isempty(self.Channels{k})
                    res(end+1) = k; %#ok<AGROW>
                end
            end
        end
        
        function res = channelByTitle(self, title)
        % channelByTitle - Find channel number by title.
            for k = 1:length(self.Channels)
                if ~isempty(self.Channels{k})
                    if strcmp(self.Channels{k}.Title, title)
                        res = k;
                        return
                    end
                end
            end
            res = -1;
        end
        
        function res = channelTitles(self)
        % channelTitles - Get a list of channel titles.
            res = {};
            for k = 1:length(self.Channels)
                if ~isempty(self.Channels{k})
                    res{end+1} = self.Channels{k}.Title; %#ok<AGROW>
                end
            end
        end
        
        function res = channelTypes(self)
        % channelTypes - Get list of channel types.
            res = {};
            for k = 1:length(self.Channels)
                if ~isempty(self.Channels{k})
                    res{end+1} = self.Channels{k}.Type; %#ok<AGROW>
                end
            end
        end

        function res = getByChannel(self, ch, progress_cb)
        % getByChannel - Get channel data by channel number.
            if (ch > length(self.Channels)) || (ch < 1)
                res = [];
                return
            end
            
            if nargin < 3
                progress_cb = [];
            end
        
            channel = self.Channels{ch};
            if isempty(channel)
                res = [];
            else
                res = struct();
                res.Type = channel.Type;
                res.Port = channel.Port;
                res.Title = channel.Title;
                res.Unit = channel.Unit;
                res.Comment = channel.Comment;
                
                res.Data = channel.RawDataCB(progress_cb);
                switch channel.Type
                    case 'Waveform'
                        res.Data.y = double(res.Data.y) * channel.UnitPerBit * channel.Scale - channel.Offset;
%                     case 'Event-',
%                     case 'Event+',
%                     case 'Level',
%                     case 'Marker',
%                     case 'TextMarker',
%                     case 'WaveMark',
                end
                
            end
        end

        function res = get(self, ch, varargin)
        % get - Get channel data.
        % 
        % USAGE
        %   ans = obj.get('*', 'Property1', property_value1, ...)
        %       Get all active channels (returns a cell).
        %   ans = obj.get('channel-name', 'Property1', property_value1, ...)
        %       Get channel by channel title.
        %   ans = obj.get(num, 'Property1', property_value1, ...)
        %       Get channel by channel number.
        %
        % PROPERTIES
        %   Progress    @(i, N, msg)     Progress callback.
            options = proplist.parse({...
                { 'Progress', 'progress_cb', []}, ...
            }, true, varargin);
        
            if ischar(ch)
                switch ch
                    case '*'
                        res = {};
                        for k = 1:length(self.Channels)
                            channel = self.Channels{k};
                            if ~isempty(channel)
                                res{end+1} = self.getByChannel(k, options.progress_cb); %#ok<AGROW>
                            end
                        end
                    otherwise
                        res = [];
                        
                        for k = 1:length(self.Channels)
                            if strcmp(self.Channels.Title, ch)
                                res = self.getByChannel(k, options.progress_cb);
                            end
                        end
                end
            else
                res = self.getByChannel(ch, options.progress_cb);
            end
        end
    end
    
    %% Tools
    methods(Static)
        function t = channelType(id)
        % CHANNELTYPE - Get channel type based on channel ID.
            % FIXME missing types: Event-, Level, WaveMark
            switch id
                case 0
                    t = 'Off';
                case 1
                    t = 'Waveform';
                case 2
                    t = 'Event-';
                case 3
                    t = 'Event+';
                case 4
                    t = 'FIXME';      % Level ?
                case 5
                    t = 'Marker';
                case 6
                    t = 'FIXME';
                case 7
                    t = 'FIXME';
                case 8
                    t = 'TextMarker';
                otherwise
                    t = 'UNKNOWN';
            end
        end
    end
end