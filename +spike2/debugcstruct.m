function s = debugstruct(s) %#ok<FNDEF>
    fn = fieldnames(s);

    for f = 1:length(fn)
        field = char(fn(f));
        if ~isempty(regexp(field, '^x[0-9A-F][0-9A-F][0-9A-F][0-9A-F]$', 'once'))
            h = s.(field);
            if isa(h, 'uint8')
                s.(field) = sprintf('%02X ', h);
            end
        end
    end
end
