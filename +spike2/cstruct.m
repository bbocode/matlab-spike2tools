function res = cstruct(fmt,seq,pos,seqlen)
% CSTRUCT - Convert byte sequence (uint8) to a matlab structure.
%
% USAGE:
% res = cstruct(<definition>, <byte-sequence>[, <start-position>, [, <sequence-length>]])
% 
% Definition is a cell of entries of the form:
%     { 'field-name', 'matlab-type', 'c-type', times }
    res = struct();

    % need row format
    if size(seq,2) == 1
        seq = seq';
    end

    % sequence start position
    if nargin < 3
        pos = 1;
    end

    % number of bytes
    if nargin < 4
        seqlen = size(seq, 2);
    end

    % loop over definitions
    for k = 1:length(fmt)
        def = fmt{k};

        fieldname = def{1};
        mtype     = def{2};
        ctype     = def{3};
        ccount    = def{4};

        switch lower(ctype)
            case {'uint8', 'int8'}
                csizeof = 1;
            case {'uint16', 'int16'}
                csizeof = 2;
            case {'uint32', 'int32', 'single'}
                csizeof = 4;
            case {'uint64', 'int64', 'double'}
                csizeof = 8;
            otherwise
                error(['Format ''' datatype ''' not known or unimplemented.']);
        end

        nextpos = pos + csizeof*ccount;
        if nextpos-1 > seqlen
            error('Structure exceeds length of input.');
        end

        res.(fieldname) = cast(typecast(seq(pos:nextpos-1), ctype), mtype);
        pos = nextpos;
    end

    if pos <= seqlen
        warning('Structure shorter than length of input.');
    end
end
